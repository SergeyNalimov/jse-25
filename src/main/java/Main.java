import tm.entity.Description;
import tm.entity.Person;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        List<Object> objects = new ArrayList<>();
        objects.add(new Person("Имя1", "Фамилия1", LocalDate.of(1950, 10, 15), "mail1@mail.com"));
        objects.add(new Person("Имя2", "Фамилия2", null, "mail2@mail.com"));
        objects.add(new Person("Имя3", "Фамилия3"));
       // System.out.println(objects);
        Reflector ref = new Reflector();
        try {
            for (List<Description> descriptions: ref.getFields(objects)) {
                for (Description description: descriptions) {
                    System.out.println(description);
                }
                System.out.println();
            }
            ref.serializeToCsv(objects);
        } catch (IllegalAccessException | IllegalArgumentException e) {
            logger.severe(e.getMessage());
        }
    }

}

