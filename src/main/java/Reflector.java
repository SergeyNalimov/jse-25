import tm.entity.Description;

import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Reflector {

    private static final Logger logger = Logger.getLogger(Reflector.class.getName());

    private static final String CSV_FILE = "csv.csv";

    public void serializeToCsv(final List<Object> objects) throws IllegalAccessException {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(CSV_FILE), StandardCharsets.UTF_8)) {
            List<List<Description>> fields = getFields(objects);
            // Header
            if (!fields.isEmpty()) {
                String parameters = fields.get(0).stream().map(Description::getParameterName).collect(Collectors.joining(","));
                writer.write(parameters);
                writer.newLine();
            }
            // Body
            for (List<Description> descriptions: fields) {
                String values = descriptions.stream().map(e -> e.getHasValue() ? e.getValue().toString() : "").collect(Collectors.joining(","));
                writer.write(values);
                writer.newLine();
            }
        } catch (IOException e) {
            logger.severe(e.getMessage());
        }
    }


    public List<List<Description>> getFields(final List<Object> objects) throws IllegalAccessException {
        List<List<Description>> result = new ArrayList<>();
        Object firstObject = objects.stream().findFirst().orElse(null);
        for (Object object: objects) {
            if (!object.getClass().equals(firstObject.getClass())) {
                throw new IllegalArgumentException("Invalid object type.");
            }
            result.add(getObjectFields(object, object.getClass()));
        }
        return result;
    }

    private List<Description> getObjectFields(final Object object, final Class objectClass) throws IllegalAccessException {
        if (object == null || objectClass == null) {
            return Collections.emptyList();
        }
        Set<Description> descriptions = new TreeSet<>();
        for (Field field: objectClass.getDeclaredFields()) {
            field.setAccessible(true);
            final String fieldName = field.getName();
            final String fieldTypeName = field.getType().getSimpleName();
            final Object value = field.get(object);
            final boolean hasValue = value != null;
            descriptions.add(new Description(fieldName, fieldTypeName, hasValue, value));
            descriptions.addAll(getObjectFields(object, objectClass.getSuperclass()));
        }
        return new ArrayList<>(descriptions);
    }

}
